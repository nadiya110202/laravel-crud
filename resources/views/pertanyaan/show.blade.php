@extends('adminlte.master')

@section('content')

<div class="mx-3 mt-4">
    <h4>Judul   : {{$pertanyaan->judul}}</h4> <br>
    <h6>Isi     : {{$pertanyaan->isi}}</h6>
    <a href="/pertanyaan" class="btn btn-primary mt-3">Back</a>
</div>

@endsection