@extends('adminlte.master')

@section('content')

<div class="container">
<a href="/pertanyaan/create" class="btn btn-primary mt-3 mb-3 mr-3">Tambah Pertanyaan</a>

<table class="table mr-3">
  <thead class="thead-light">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Judul</th>
      <th scope="col">Isi</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($pertanyaan as $key=>$value)
    <tr>
      <td>{{$key + 1}}</td>
      <td>{{$value->judul}}</td>
      <td>{{$value->isi}}</td>
      <td>
        <form action="/pertanyaan/{{$value->id}}" method="POST">
            <a href="/pertanyaan/{{$value->id}}" class="btn btn-info">Show</a>
            <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-warning">Edit</a>
            @csrf 
            @method('DELETE')
            <input type="submit" class="btn btn-danger my-1" value="Delete">
        </form>
      </td>
    </tr>
    @empty
        <tr colspan="3">
            <td>No Data</td>
        </tr>
    @endforelse
  </tbody>
</table>
</div>

@endsection